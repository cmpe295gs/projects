## Table of Contents

### Project Overview
- Idea and Motivation

### Design Methodology
- State of the Art - Blockchain.
- Distributed File sharing methods.
- Compelling cases - Need for a blockchain based Digital Library

### Functional Architecture
 - Use Cases
 - Class Diagrams

### Technical Architecture 
- Frontend Architecture
- Backend Architecture

### Project Plan and Execution
- Implementation Plan

### Testing, Performance and Scalability
- Unit Testing
<ul>1. Testing Frontend components</ul>
<ul> 2. Testing Backend components</ul>

- System Integration Testing
<ul>1. Testing Frontend components</ul>
<ul>2. Testing Backend components</ul>

- Load Testing
<ul>1. Testing Frontend components</ul>
<ul>2. Testing Backend components</ul>

### Project Costs and Deployment Methods
- On premise deployments.
- Cloud based solutions.

### Operations 
- Activities for on premise solution.
- Activities for a cloud based solution.

### Summary and Conclusions

### Recommendation and Future Scope.



