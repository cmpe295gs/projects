##### IPFS node setup

1. IPFS node can be setup locally.
2. Install the ipfs-go package [link](https://docs.ipfs.io/introduction/install/)
3. Setup data directories.
4. Expose the ports to communicate with IPFS.
5. Communication includes 'add' 'get' documents.
