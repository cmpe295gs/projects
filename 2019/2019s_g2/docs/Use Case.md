#### Use Cases for the application

Sr.No | Use case | Description 
-|-|-|
1 | Registration of a new entity into the platform | This is where a new contributing entity(library, publishing house) is added to the platform. these entities are the power users.
2 | Addition of new book onto the platform | New books can be added either by the library or a publishing house. ^
3 | Registration for end-users who can use the platform | Enable a new user to use the platform. Only registered users can use the platform
4 | Removal of books from the platform | Remove books so that they no longer appear to the end user. Note that it is still available to the platform power users.
5 | Login to the platform | Only registered power users and end-users can login into the platform.
6 | View book on the platform | Both power users and end-users must be able to view the books
7 | Edit books on the platform | Editing a book ~
8 | Search books on the platfrom | Any registered user can search books on the platform
9 | Borrow books | Any registered user can borrow books on the platform $
10 | Return books | Borrowed books having limited time to use need to be returned. 


Notes:
- ^ In phase 1 we will consider only libraries and publishing houses as power users and not individual users.
- ~ Books can be modified by power users will be treated as addition of a new book.
- $ Max borrow limit will be set to 10.


Item | Description
-|-|
UC01 | Registration of a new entity into the platform
Actors | Power users, platform admin
Brief Desc | The platform admin will add the power-users into the network.
Pre-conditions | Power users must be authorized externally.
Post-conditions | Once the power-user is added to the platform they can start adding digital books.

Item | Description
-|-|
UC02 | Addition of new book onto the platform
Actors | Power users
Brief Desc | Addition of books into the platform
Pre-conditions | Power users must be authorized by the system
Post-conditions | Once the asset is added and accepted to the chain a confirmation must be sent.

Item | Description
-|-|
UC03 | Registration for end-users who can use the platform.
Actors | End users
Brief Desc | The end user can use the registration page on the portal to complete the registration.
Pre-conditions | End users must be valid users verified using capatcha.
Post-conditions | Once the power-user is added to the platform they can start adding digital books.

Item | Description
-|-|
UC04 | Removal of books from the platform
Actors | Power users, platform admin
Brief Desc | The power-users must be able to remove the books from the platform, i.e make it unavailble to end users.
Pre-conditions | Books present on the platform and available for borrowing.
Post-conditions | Books that are removed will not be available to the end-user on the platform anymore.

Item | Description
-|-|
UC05 | Login into the platform.
Actors | Power users, platform admins, end-users.
Brief Desc | Platform can be used by registered users only.
Pre-conditions | Platform allows for password protected login feature.
Post-conditions | The users are allowed access only if the username/password combination is correct.

Item | Description
-|-|
UC06 | View books on the platform.
Actors | Power users, platform admins, end-users.
Brief Desc | Registered users can view the books available on the platform.
Pre-conditions | Platform allows for password protected login feature.
Post-conditions | The users are allowed access only if the username/password combination is correct.

Item | Description
-|-|
UC07 | Edit books on the platform.
Actors | Power users.
Brief Desc | Power users can edit the books available on the platform.
Pre-conditions | Platform allows for power users to password protect login feature.
Post-conditions | The books that are edited will be available to end-users only after consensus is reached.

Item | Description
-|-|
UC08 | Search books on the platform.
Actors | Power users, platform admins, end-users.
Brief Desc | Registered users can search all books available on the platform.
Pre-conditions | Platform will allow users to browse through categories and search books.
Post-conditions | The users can use the serach feature on the platform.

Item | Description
-|-|
UC09 | Borrow books on the platform.
Actors | Power users, platform admins, end-users.
Brief Desc | Registered users can borrow the books available on the platform.
Pre-conditions | Books that are available can be borrowed.
Post-conditions | Books that are borrowed on the platform will have record for the borrow event.

Item | Description
-|-|
UC10 | Return books on the platform.
Actors | Power users, platform admins, end-users.
Brief Desc | Registered users can return the books borrowed initially on the platform.
Pre-conditions | Borrowed books can be returned.
Post-conditions | Platform will maintain a record for the fully cycle of books during the borrow and return phase.


