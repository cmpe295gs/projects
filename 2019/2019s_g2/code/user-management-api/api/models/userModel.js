'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = new Schema({
  UserID: {
    type: String,
    required: 'Enter UserID '
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  HashCode: {
    type: String,
    required: 'Enter HashCode '
  }
});

module.exports = mongoose.model('Users', UserSchema);