from flask import Flask, request, jsonify
from flask_restful import Resource, Api
import requests

IPFS_BASE_URL = "http://localhost:5001"
IPFS_API_VER = "api/v0"

app = Flask(__name__)
api = Api(app)


@app.route('/status')
def serverstatus():
    return jsonify({"status":"OKAY", "msg":"Server available."})

@app.route('/addtonetwork')
def addtonetwork(self):
    pass


class ipfs(Resource):
    def get(self):
        return {'hello': 'world'}

    def post(self, operation, fpath):
        url = IPFS_BASE_URL+IPFS_API_VER+operation
        files = {'file': open(fpath,'rb')}
        print(url)
        print(files)
        response = requests.post(url, files=files)
        print(response)


api.add_resource(ipfs, '/')

# TODO
# curl -F "text=@/Users/XXX/XXX_ipfs_file_get/newson.txt" 127.0.0.1:5001/api/v0/add
# Sending complete file to the api.
# Sending file path to the api.


if __name__ == '__main__':
    app.run(port='5002')
# 
