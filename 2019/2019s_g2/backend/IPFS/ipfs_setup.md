Refer to docs section for the IPFS setup.

##### IPFS container using ipfs-go implementation.
- Your host should be running Docker Engine. 

- Create the following directories and make sure to add the *path variables* for init.
```
mkdir -p ~/.ipfs
mkdir -p ~/.ipfs/staging
```

- Following commands
```
docker pull ipfs/go-ipfs

docker run -d --init \
		--name ipfs_host \
		-v $ipfs_staging:/export \
		-v $ipfs_data:/data/ipfs \
		-w /export \
		-p 4001:4001 \
		-p 127.0.0.1:8080:8080 \
		-p 127.0.0.1:5001:5001 \
		ipfs/go-ipfs:latest
```

- Following commands can be used to add the files to the IPFS network.

```
docker exec ipfs_host ipfs add -r /export/<complete_path_of_the_file>
```

- ipfs-go also allow interaction with the IPFS network using APIs. Following is a sample curl commands to interact with the api.

```
curl -F "image=@/home/bar.jpg" 127.0.0.1:5001/api/v0/add
curl -F file=@myfile "http://localhost:5001/api/v0/add
```

- Following can be used to add the data to the file directly using a form element.

```
<form action="http://127.0.0.1:5001/api/v0/add">
  <input type="file" name="image" accept="image/*">
  <input type="submit">
</form>
```

- Please use the postman collections file to work for saved api interactions.

- Additonal references : [IPFS/GO container](https://hub.docker.com/r/ipfs/go-ipfs) ; [IPFS_HTTP reference](https://docs.ipfs.io/reference/api/http/)


