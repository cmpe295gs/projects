package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

type Block struct {
	Index     int
	Timestamp string
	fileHash  string
	Hash      string
	PrevHash  string
}

type Message struct {
	fileHashstring string
}

var mutex = &sync.Mutex{}

var Blockchain []Block

/*
Blockchain component and the related functions.

calculateHash(block) string  						: Will calculate the hash for a new block.
isBlockValid(newBlock, oldBlock) bool  				: Will return true if block is valid.
createNewBlock(oldBlock, fileHash) Block, error		: Generate the next new block based on inputs (fileHash)
longestChain(newBlocks) []Block						: returns the chain will longest length in case two chains recieved by the node at the same time.
*/

func calculateHash(block Block) string {
	record := string(block.Index) + block.Timestamp + block.fileHash + block.PrevHash
	h := sha256.New()
	h.Write([]byte(record))
	hashed := h.Sum(nil)
	return hex.EncodeToString(hashed)
}

func createNewBlock(oldBlock Block, fileHash string) Block {

	fmt.Println("===FILEHASH-===")
	fmt.Println("filehash: ", fileHash)
	fmt.Println("===========")
	// fmt.Println("")

	var newBlock Block

	t := time.Now()

	newBlock.Index = oldBlock.Index + 1
	newBlock.Timestamp = t.String()
	newBlock.fileHash = fileHash
	newBlock.PrevHash = oldBlock.Hash
	newBlock.Hash = calculateHash(newBlock)

	return newBlock
}

func isBlockValid(newBlock, oldBlock Block) bool {
	//validating based on index.
	if oldBlock.Index+1 != newBlock.Index {
		return false
	}

	//validating based on previous hash and current hash.
	if oldBlock.Hash != newBlock.PrevHash {
		return false
	}

	//validating based on calculated hashes.
	if calculateHash(newBlock) != newBlock.Hash {
		return false
	}

	return true
}

func replaceChain(newBlock []Block) {
	//assuming that the largest chain is the most updated one. i.e has more latest records
	if len(newBlock) > len(Blockchain) {
		Blockchain = newBlock
	}
}

/*
Build the web server components and related functions.

webServer()				: Webserver using gorilla mux.
webServerRouteHandler()	: Route handlers.
getBlockchain()			: Handling the GET requests & return the current chain.
writenewBlock()			: Handling the POST request to create a new block and if valid add to the current chain
jsonResponse()			: helper function to respond with JSON object.
*/

func webServer() error {
	mux := webServerRouteHandler()
	httpAddr := os.Getenv("HTTP_PORT")
	log.Println("Blockchain server listening on: ", os.Getenv("HTTP_PORT"))
	s := &http.Server{
		Addr:           ":" + httpAddr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if err := s.ListenAndServe(); err != nil {
		return err
	}

	return nil
}

func webServerRouteHandler() http.Handler {
	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/getBlockchain", getBlockchain).Methods("GET")
	muxRouter.HandleFunc("/writenewBlock", writenewBlock).Methods("POST")
	return muxRouter
}

func getBlockchain(w http.ResponseWriter, r *http.Request) {
	bytes, err := json.MarshalIndent(Blockchain, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.WriteString(w, string(bytes))
}

func writenewBlock(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	var msg Message
	fmt.Println("======")
	fmt.Println(msg)
	log.Println(msg)

	decoder := json.NewDecoder(r.Body)
	fmt.Println("====DECODER===", decoder)
	if err := decoder.Decode(&msg); err != nil {
		fmt.Println("====BODY===", r.Body)
		jsonResponse(w, r, http.StatusBadRequest, r.Body)
		return
	}
	fmt.Println("====BODY===", r.Body)
	defer r.Body.Close()

	//create a new Block.
	// mutex.Lock()
	prevBlock := Blockchain[len(Blockchain)-1]
	newBlock := createNewBlock(prevBlock, msg.fileHashstring)
	fmt.Println("=========NEW_BLOCK: ", newBlock)

	// newBlock, err := createNewBlock(Blockchain[len(Blockchain)-1], msg.fileHash)
	// if err != nil {
	// 	jsonResponse(w, r, http.StatusInternalServerError, msg.fileHash)
	// 	return
	// }

	//if new block is valid, add it to the current chain.
	if isBlockValid(newBlock, Blockchain[len(Blockchain)-1]) {
		newBlockchain := append(Blockchain, newBlock)
		replaceChain(newBlockchain)
		//spew will pretty print the chain to the console.
		spew.Dump(Blockchain)
	}
	//return the added block.
	fmt.Println("=========NEW_BLOCK: ", newBlock)
	jsonResponse(w, r, http.StatusCreated, newBlock)
}

func jsonResponse(w http.ResponseWriter, r *http.Request, code int, payload interface{}) {
	fmt.Println("====PAYLOAD===", payload)
	response, err := json.MarshalIndent(payload, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("HTTP 500: Internal Server Error"))
		return
	}
	w.WriteHeader(code)
	w.Write(response)
}

/*
The main function and components.
1. Load env details eg. port number.
2. Init the blockchain.
3. start the blockchain webserver.

*/

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		t := time.Now()
		genesisBlock := Block{}
		genesisBlock = Block{0, t.String(), "", calculateHash(genesisBlock), ""}
		spew.Dump(genesisBlock)

		mutex.Lock()
		Blockchain = append(Blockchain, genesisBlock)
		mutex.Unlock()
	}()
	log.Fatal(webServer())

}

