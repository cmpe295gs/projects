const mongoose = require('mongoose');
const bcrypt = require('bcrypt')
const UserSchema = new mongoose.Schema({
  firstName: {
    type: String,
    default: ""
  },
  lastName: {
    type: String,
    default: ""
  },
  email: {
    type: String,
    default: ""
  },
  password: {
    type: String,
    default: ""
  },
  isDeleted:{
    type: Boolean,
    default: false
  },
  HashCode: {
    type: String,
    default:""
  }
});



UserSchema.methods.validPassword = function(password){
  if (password == this.password){
    return true
  }
  else{
    return false
  }
}

module.exports = mongoose.model('User', UserSchema);
