import React from 'react';
import { render } from 'react-dom';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom'

import App from './components/App/App';
import NotFound from './components/App/NotFound';
import Home from './components/Home/Home';
import HomeContainer from './Containers/HomeContainer/HomeContainer'
import RequestPublishContainer from './Containers/RequestPublishContainer/RequestPublishContainer'
import GetAssetContainer from './Containers/GetAssetContainer/GetAssetContainer'
import AboutUs from './components/AboutUs/AboutUs'
import './styles/styles.scss';
import ContactUs from './components/ContactUs/ContactUs';
import UserDetails from './components/UserDetails/UserDetails';

render((
  <Router>
    <App>
      <Switch>
        <Route exact path="/" component={HomeContainer}/>
        {/* <Route path="/helloworld" component={HelloWorld}/> */}
        {/* <Route component={NotFound}/> */}
        <Route exact path="/getAsset" component={GetAssetContainer}/>
        <Route exact path="/requestPublish" component={RequestPublishContainer}/>
        <Route exact path="/about" component={AboutUs}/>
        <Route exact path="/contact" component={ContactUs}/>
        <Route exact path="/userDetails/" component={UserDetails}/>

      </Switch>
    </App>
  </Router>
), document.getElementById('app'));
