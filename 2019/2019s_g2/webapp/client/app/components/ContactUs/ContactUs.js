import React, { Component } from "react";
import Button from '@material-ui/core/Button';
import './ContactUs.css'
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBIcon, MDBInput } from 'mdbreact';
import { FaBeer } from 'react-icons/fa';
import Form, { Input, Fieldset } from 'react-bootstrap-form';

class ContactUs extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
<div>
<ul>
  <li><a class="active" href="/">Home</a></li>
    <li><a  href="/AboutUs">About Us</a></li>
        <li><a  href="/RequestPublish">Request Publish</a></li>
  <li class="a"><a href="/ContactUs">Contact Us</a></li>
</ul>
 <div>
</div>

    <MDBContainer>
      <MDBRow>
        <MDBCol md="12">
          <form>
           <div>
</div> 
            <p className="h1 text-center mb-4">GET IN TOUCH WITH US
</p>
            <div className="grey-text">
              <MDBInput
                label="Your name"
                icon="user"
                group
                type="text"
                validate
                error="wrong"
                success="right"
              />
              <MDBInput
                label="Your email"
                icon="envelope"
                group
                type="email"
                validate
                error="wrong"
                success="right"
              />
              <MDBInput
                label="Subject"
                icon="tag"
                group
                type="text"
                validate
                error="wrong"
                success="right"
              />
              <MDBInput
                type="textarea"
                rows="2"
                label="Your message"
                icon="pencil-alt"
              />
            </div>
            <div className="text-center">
              <MDBBtn outline color="secondary">
                Send <MDBIcon far icon="paper-plane" className="ml-1" />
              </MDBBtn>
            </div>
          </form>
        </MDBCol>
      </MDBRow>
    </MDBContainer>

  </div>
    
    );
  }
}


export default ContactUs;