import React, { Component } from "react";
import Button from '@material-ui/core/Button';
import './AboutUs.css'


class AboutUs extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (



<div>

<ul>
  <li><a class="active" href="/">Home</a></li>
    <li><a  href="/AboutUs">About Us</a></li>
        <li><a  href="/RequestPublish">Request Publish</a></li>
  <li class="a"><a href="ContactUs">Contact Us</a></li>

</ul>


<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>Our Blog</h4>

   
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Blog.."/>


        <span class="input-group-btn">
          <button class="btn btn-default" type="button">

            <span class="glyphicon glyphicon-search"></span>
          </button>

        </span>
<br/>
      </div>
<br/><br/>

<h5>Use the search form here to locate resources from Blockchain Library.</h5>
<br/><br/><br/><br/>

<h4>RECENT POSTS</h4>
<br/><br/>
<h4>Most Cited Behavioral Economics Publications</h4>
      <hr/>

<h4>Most Cited Articles about Financial Bubbles</h4>
      <hr/>

<h4>Most Cited Cryptocurrency Regulation Articles Recently Published</h4>
      <hr/>

<h4>Most Cited Privacy Articles for Blockchain and Cryptocurrency, zkSnarks, or RingCT Recently Published</h4>
      <hr/>

<h4>Most Cited Publications for Stablecoins, Blockchain, and Cryptocurrency</h4>

    </div>




    <div class="col-sm-9">
    <b>  <h1>Who are we and,
why should you care ?</h1>
      <hr/></b>

      <br/>     <br/>     <br/>
      <h2>The Global Leader of Blockchain of Digital Libraries Solutions</h2>
<br/>
      <p>

The Blockchain Library’s goals are twofold: to provide easy access to resources on cryptocurrency research for scholars, academics, analysts, and anyone with an interest in cryptocurrency publications; the second is to provide an archive and historical timeline on the development of the blockchain field and to document the journey of its growth through preservation and archival projects.
 </p>
      <br/><br/>


<img src="http://bablaniaircon.com/wp-content/uploads/2018/11/AboutUs_Leadership_0.png" class="img-circle"   alt="Avatar"/>

      <br/><br/>
      
     <b> <h1><small>ABOUT US</small></h1></b>
      <hr/>
      
      <h5><span class="label label-success"></span></h5><br/>

<p>
We propose a blockchain approach to create a chain of different digital libraries to support our members in a secured way. Our mission is to provide a digital library and archive of resources on blockchain and cryptocurrencies for researchers worldwide.
</p>

<br/>

<p>

<b>Philosophy, Ethics, and Principles</b>
<br/>
<br/>

Finally, we care about principles such as security, privacy, and open access to information. We want to share information to researchers in such a way there are no compromises with security or privacy. This is why we run the site in secure settings. We also believe that this open access philosophy can provide the mechanism to most widely transmit information to future innovators and creators.
</p>

<br/> <br/>
<br/> <br/>

      <hr/>
<img src="https://d1v9sz08rbysvx.cloudfront.net/careers/media/heroes/connected.png?ext=.png" class="img-circle"   alt="Avatar"/>


<br/> <br/>
<br/> <br/>
<br/> <br/>
      <h4>Leave a Comment:</h4>
      <form role="form">
        <div class="form-group">
          <textarea class="form-control" rows="3" required></textarea>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
      </form>
      <br/><br/>
      
      <p><span class="badge">2</span> Comments:</p><br/>
      
      <div class="row">
        <div class="col-sm-2 text-center">
          <img src="https://cdn-images-1.medium.com/max/1200/1*3shOJNSwkZIl07R1n9nVUw.jpeg" class="img-circle" height="65" width="65" alt="Avatar"/>
        </div>
        <div class="col-sm-10">
          <h4>Anja <small>Sep 29, 2015, 9:12 PM</small></h4>
          <p>Keep up the GREAT work! I am cheering for you!! Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <br/>
        </div>
        <div class="col-sm-2 text-center">
          <img src="https://cdn-images-1.medium.com/max/1200/1*3shOJNSwkZIl07R1n9nVUw.jpeg" class="img-circle" height="65" width="65" alt="Avatar"/>
        </div>
        <div class="col-sm-10">
          <h4>John Row <small>Sep 25, 2015, 8:25 PM</small></h4>
          <p>I am so happy for you man! Finally. I am looking forward to read about your trendy life. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <br/>
          <p><span class="badge">1</span> Comment:</p><br/>
          <div class="row">
            <div class="col-sm-2 text-center">
              <img src="https://cdn-images-1.medium.com/max/1200/1*3shOJNSwkZIl07R1n9nVUw.jpeg" class="img-circle" height="65" width="65" alt="Avatar"/>
            </div>
            <div class="col-xs-10">
              <h4>Nested Bro <small>Sep 25, 2015, 8:28 PM</small></h4>
              <p>Me too! WOW!</p>
       
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<footer class="container-fluid">
<br/><br/><br/>
<p>Copyright © 2019 Blockchain of Digital Libraries Privacy Policy | Terms of Service</p>
</footer>





</div>


      

    );
  }
}




export default AboutUs;