import React, { Component } from 'react';
import 'whatwg-fetch';
import { withRouter } from 'react-router-dom';
import { browserHistory } from 'react-router';
import Blockchain_of_Digital_libraries_Final_Report from '../../files/Blockchain_of_Digital_libraries_Final_Report.pdf'
import Wireframes from '../../files/Wireframes.pdf'



class UserDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
           
        };

       
    }

  

    render() {
        console.log(this.props.history.location.data.firstName, "this.props")
            return (
                <div>
                    <div style={{
                        'background-image': 'url("https://dt.azadicdn.com/wp-content/uploads/2013/09/digital-book.jpg?200")',
                        'min-height': '100vh',
                        '-webkit-background-size': 'cover',
                        '-moz-background-size': 'cover',
                        '-o-background-size': 'cover',
                        'background-size': 'cover',
                    }}>
                    <div style={{'display':'flex', 'justify-content':'space-evenly'}}>
                    
                  <label style ={{'color':'white', "margin-top":'120px'}}>First Name : {this.props.history.location.data.firstName}</label>
                  <label style ={{'color':'white', "margin-top":'120px'}}>Last Name : {this.props.history.location.data.lastName}</label>
                  <label style ={{'color':'white', "margin-top":'120px'}}>Email : {this.props.history.location.data.email}</label>
                  
                    </div>
                    <label style ={{'color':'white', "margin-top":'120px', 'display': 'flex', 'justify-content':'center'}}>Assets: </label>
                    <label ><a style ={{'color':'white', "margin-top":'10px', 'display': 'flex', 'justify-content':'center'}} download href={Blockchain_of_Digital_libraries_Final_Report}> Download Blockchain_of_Digital_libraries_Final_Report</a></label>
                    <label ><a style ={{'color':'white', "margin-top":'10px', 'display': 'flex', 'justify-content':'center'}} download href={Wireframes}>Download Wireframes</a></label>
                    </div>

                </div>
            );
        }

       
    }


export default withRouter(UserDetails);