import React, { Component } from 'react';
import axios from 'axios'
import 'whatwg-fetch';
import Button from '@material-ui/core/Button';
import SignUp from "../../components/SignUp/SignUp"
import SignIn from "../../components/SignIn/SignIn"
import './GetAssetContainer.css'
import { withRouter } from 'react-router-dom';
import { browserHistory } from 'react-router';
import {
    getFromStorage,
    setInStorage,
} from '../../utils/storage';
import FileUpload from '../../components/FileUpload/FileUpload';
import CustomizedTable from '../../components/CustomizedTable/CustomizedTable';
import { func } from 'prop-types';

class GetAssetContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users:[],
            user:{}
        };
        this.userClicked = this.userClicked.bind(this)
    }

    componentDidMount(){
        axios.get(`http://ec2-18-217-9-12.us-east-2.compute.amazonaws.com:3000/users/`, {
           headers: {
               'Access-Control-Allow-Origin': '*',
           },
       })
           .then((response) => {
               console.log(response);
               this.setState({users:response.data})
           })
           .catch(function (error) {
               console.log(error);
       });
   }

    userClicked(user){
        axios.get(`http://ec2-18-217-9-12.us-east-2.compute.amazonaws.com:3000/users/${user._id}`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then( (response) => {
                this.setState({user:response})
                console.log(response);
            }).then((response) => {
                this.props.history.push({
                    pathname: '/userDetails/',
                    data:  this.state.user.data 
                  })
            })
            .catch(function (error) {
                console.log(error);
        });
    }
  
    render() {
        return (
            <div>
                <div style={{
                    'background-image': 'url("https://dt.azadicdn.com/wp-content/uploads/2013/09/digital-book.jpg?200")',
                    'min-height': '100vh',
                    '-webkit-background-size': 'cover',
                    '-moz-background-size': 'cover',
                    '-o-background-size': 'cover',
                    'background-size': 'cover',
                }}>
                    <div style={{ 'display': 'flex', 'justify-content': 'space-evenly' }}>
                    <CustomizedTable userClicked={this.userClicked.bind(this)} data={this.state.users}/>
                    </div>
                </div>

            </div>
        );
    }


}


export default withRouter(GetAssetContainer);