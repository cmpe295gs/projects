import React, { Component } from 'react';
import axios from 'axios'
import 'whatwg-fetch';
import Button from '@material-ui/core/Button';
import SignUp from "../../components/SignUp/SignUp"
import SignIn from "../../components/SignIn/SignIn"
import './RequestPublishContainer.css'

import {
    getFromStorage,
    setInStorage,
} from '../../utils/storage';
import FileUpload from '../../components/FileUpload/FileUpload';

class RequestPublishContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };

        this.handleFileUpload = this.handleFileUpload.bind(this)
        this.clickHandler = this.clickHandler.bind(this)
    }

    clickHandler() {
        const handleFileUpload = document.getElementById("file-upload")
        handleFileUpload.click()
    }

    // handleFileUpload(event) {
    //     let filename = ''
    //     //fix me
    //     if (event.target.files.length === 0) return
    //     filename = event.target.files[0].name;


    //     const obj = getFromStorage('the_main_app');
    //     console.log("obj", obj)
    //     console.log(event.target.files[0])
    //     if (filename) {



    //         const data = new FormData()
    //         data.append('templateData', event.target.files[0])
    //         data.append('templateID', this.props.templateId)
    //         console.log(data, "data")



    //         axios.post('/api/account/upload',
    //             formData,
    //             {
    //                 headers: {
    //                     'Content-Type': 'multipart/form-data'
    //                 }
    //             }
    //         ).then(function () {
    //             console.log('SUCCESS!!');
    //         })
    //             .catch(function () {
    //                 console.log('FAILURE!!');
    //             });


    //     }
    // }

    handleFileUpload(event) {
        console.log(event.target.files[0], "file")
        axios.post('http://127.0.0.1:5001/api/v0/add', event.target.files[0])
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
        // const item = {
        //     "HashCode":["ttt"]
        // }
        //  axios.put(`http://ec2-18-217-179-59.us-east-2.compute.amazonaws.com:3000/users/5cc80f90d5596129ffa0807e`, item).then(response => {
        //     console.log(response)
        // })
        // axios.get(`http://ec2-18-217-179-59.us-east-2.compute.amazonaws.com:3000/users/5cc80f90d5596129ffa0807e`, {
        //     headers: {
        //         'Access-Control-Allow-Origin': '*',
        //     },
        // })
        //     .then(function (response) {
        //         console.log(response);
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        // });
    }
    render() {
        return (
            <div>
                <div style={{
                    'background-image': 'url("https://dt.azadicdn.com/wp-content/uploads/2013/09/digital-book.jpg?200")',
                    'min-height': '100vh',
                    '-webkit-background-size': 'cover',
                    '-moz-background-size': 'cover',
                    '-o-background-size': 'cover',
                    'background-size': 'cover',
                }}>
                    <div style={{ 'display': 'flex', 'justify-content': 'space-evenly' }}>
                        <input type='file' id='file-upload' hidden='hide' onChange={this.handleFileUpload} />
                        <Button style={{ "margin-top": '200px', 'transform': 'scale(2)' }} variant="contained" color="primary" onClick={this.clickHandler}>Upload</Button>
                        {/* <FileUpload/> */}
                        {/* <div>

                            <form action="http://127.0.0.1:5001/api/v0/add">
                                <input type="file" name="image" accept="image/*"/>
                                <input type="submit"/>
                            </form>
                        </div> */}
                    </div>
                </div>

            </div>
        );
    }


}


export default RequestPublishContainer;