# Projects and thesis

This folder includes materials of all the CMPE 295 projects and thesis supervised under Prof. Saldamli.  
## Meeting schedule

| time         | Sep.01   | Sep.15   | Sep.29   | Oct.13   | Oct.27   |
|:------------:|:--------:|:--------:|:--------:|:--------:|:--------:|
|   2:30-3:00  |          |          |          |          |          |
|   3:00-3:30  |  21F1    |  21F1    |  21F1    |  21F1    |  21F1    |
|   3:30-4:00  |  21F2    |  21F2    |  21F2    |  21F2    |  21F2    |
|   4:00-4:30  |  21F3    |  21F3    |  21F3    |  21F3    |  21F3    |
|   4:30-5:00  |          |          |          |          |          |


| time         | Nov.10   | Nov.17   | Dec.01   | Dec.08   |          |
|:------------:|:--------:|:--------:|:--------:|:--------:|:--------:|
|   2:30-3:00  |          |          |          |          |          |
|   3:00-3:30  |  21F1    |  21F1    |  21F1    |  21F1    |          |
|   3:30-4:00  |  21F2    |  21F2    |  21F2    |  21F2    |          |
|   4:00-4:30  |  21F3    |  21F3    |  21F3    |  21F3    |          |
|   4:30-5:00  |          |          |          |          |          |

## Projects

### 2021 Fall
1. *Design and implementation of an Enterprise-Grade Wireless Network with Strong Authentication.*  
    by Abishek Mugunthan , Ajay Kumar Ravipati, Roopesha Sheshappa Rai and Vijaya Laxmi Durga Alekya Nynala.

2. *Automated Web Hosting.*  
    by Harshil Sujitkumar Shah, Aayush Sukhadia, Deepen Patel and Narain. 

3. *Drug Tracking and Recommendation System Using Blockchain and Machine Learning*
    by Amol Sadasivan, Sreeja Madanambeti, Anjali Deshmukh and Amitha Shreshta Papetla, 

### 2021 Spring
1. *Analysis of Different Distributed Cache Mechanism.*  
    by Samruddhi Landge, Mohit Patel, Prachal Jitendrakumar Patel and Priyam Vaidya.  
    (**paper in progress**)

2. *Secure Location-Based Contact Tracing for COVID-19*  
    by Sidharth Jayaprakash, Jojo Joseph, Sushant Amit Mathur and Mitra Gunakara Nayak.  
    (**paper published**, The 2021 International Conference on Security and Management (SAM'21))

3. *Analysis of Machine Learning as a Service.*  
    by Nishit Ajaykumar Doshi, Vishal Gadapa, Jainish Jitendrakumar Parikh and Mihir Maheshkumar Patel.  
    (**paper published**, The 17th International Conference on Grid, Cloud, & Cluster Computing (GCC'21))

4. *Using Convolutional Neural Networks for Material Classification*  
    by Jajodia Akansha, Lee Karen, Saraiya Hetavi and Sharma Pragati.  
    (**paper in progress**)

5. *Text Detection*  
    by DhanasreeAre, Nishant Jani, Rajeev Sebastian and Harsh Trived.  
    (**paper not complete**)

### 2020 Fall
1. *Material Classification using Neural Networks.*  
    by Naga Abhilash Reddy Julakanti, Saikumar Reddy Sandannagari, Sai Sandeep Jyothula and Sai Sarath Vattikuti.  
    (**paper in progress**)

2. *Stock prediction using neural networks*  
    by Shreya Hagalahalli, Mahesh Redddy, Sneha and Mohdi Habibi.  
    (**paper not complete**)

3. *Material science text mining using NLP.*  
    by Sarthak Sugandhi, Rohankumar Shah, Amit Kamboj and Sagar Bonde.  
    (**paper in progress**)

4. *College Navigation app using Augmented Reality.*  
    by Meghna Tiwari, Tosha Kamath, Vinit Dholakia and Sanith Katukuri.  
    (**paper not complete**)

5. *Analysis of Security and Attacks in Software-defined Networks.*  
    by Wayne Arnold, Shirin Kulkarni, Zaid Laffta and Shiva Vijay Reddy Pinreddy.  
    (**paper in progress**)

### 2020 Spring
1. *Analysis of queuing services in a distributed architecture*  
    by Viraj Upadhyay,  Atish Maitreya,  Sanjay Nag Bangalore Ravishankar and Daniel Sampreeth Reddy Eadara.  
    (**paper in progress**)

2. *Designing an investment portfolio for Public Companies using Deep Learning* 
    by Madhusudhan Reddy Shagam, Ranjith Kumar Yadav Cheguri, Sai Krishna Reddy Jali and Vinay Kovuri.  
    (**paper not complete**)

3. *Real time object detection video processing system*  
    by Sandhya Ramachandraiah, Anoop Martin, Akhil Raveendran and Naveen Ravipati.  
    (**paper not complete**)

4. *Intelligent supply chain management based on blockchain and data analytics* 
    by Anushree Menon, Ketan Rudrurkar, Ligin Thiya and Vedant Bhoj.  
    (**paper published**, Blockchain Based Vehicle Information Management System, 2020)

5. *Enterprise backend as a service*   
    by Aditya Doshatti, Darshil Kapadia, Devashish Nyati and Maulin Bodiwala.  
    (**paper published**, The 16th International Conference on Grid, Cloud, & Cluster Computing (GCC'20))

### 2019 Fall
1. *Health care insurance fraud detection using Blockchain*  
	by Krishna S. S. Bojja, Yashaswi D, Manjunatha K. Gururaja and Vamshi R. Verama.  
    (**paper published**, Seventh International Conference on Software Defined Systems (SDS), pp. 145-152, 2020)

2. *Efficient Blockchain using Gossip Protocol*  
	by Devika Jadhav, Bapugouda U. P. Patil, Rohit Shrishrimal and Charit Upadhyay.  
    (**paper submitted**)

3. *Blockchain Security and Analytics for Automotive Data*  
	by Kavitha Karunakaran, Weiyang Pan, Siddesh Puttarevaiah and Vidya K. Vijaykumar.  
    (**paper published**, Seventh International Conference on Software Defined Systems (SDS), pp. 153-159, 2020)

4. *Fitness tracking using image analysis and motion tracking*  
	by Varun Khatri, Naresh M. Kumar, Rahul Tiwari and Abraham Williams.  
    (**paper not complete**)

### 2019 Spring

1. *Identity management using blockchain*  
    by Sumedh S. Deshpande, Madhuri S. Kumar, Sohil S. Mehta and Pranjali S. Raje.  
    (**paper published**, The 2019 International Conference on Security and Management (SAM'19))

2. *Blockchain for digital libraries*  
    by Eya B. Abdisho, Shilakha Dawar, Neville Mascarenhas and Vinitkumar R. Singh.  
    (**paper not complete**)

3. *Healthcare Services Using Blockchain Technology*  
    by Karthika M. S. Nair, Roopashree Munegowda, Pavan Haravu Ramesh and Jeevan Venkataramana.  
    (**paper published**, Book Chapter, Blockchain for Cybersecurity and Privacy, CRC Press, 2020)

### 2018
1. *Malicious code detection using neural networks*  
    by Ashwini Chilukuri, Abhilash Garimella, Sphoorti Metri and
Sudha S. Ramachandran.  
    (**paper not complete**)

2. *Improving link failure recovery and congestion control in SDN networks*  
    by Rahul R. Kodati, Sahithi A. Kuntamukkala, Himanshu Mishra and Naveen Ravi.  
    (**paper published**, The 10th International Conference on Information and Communication Systems (IEEE ICICS'19))

3. *Blockchain based application for exchange of left-over foreign currency*  
    by Shreyas Babji, Miroslav Grubic, Chaitra Satyanarayana and Nischala R. Shekar.  
    (**paper published**, The First IEEE International Workshop on Blockchain Applications and Theory (IEEE BAT'19))

### 2017
1. *A secure collaborative module on distributed SDN*  
    by Ruthra V. Murugesan, Harsha Sanjeeva, Karthik Siddalingaapa.  
    (**paper published**, The 10th International Conference on Information and Communication Systems (IEEE ICICS'19))


 
## Thesis

### 2021
1. *Consensus Mechanisms in Blockchain*  
    by Prince Jassal.  

### 2020
1. *Reduction-free multiplication in gf(2n ) applicable to modern and post-quantum cryptography schemes*  
    by Samira Carolina Oliva Madrigal.  
    (**paper in progress**)


### 2019
1. *Combining blockchain and swarm robotics to deploy surveillance missions*  
    by Ardalan Razavi.  
    (**papers published**, 32nd International Conference on Microelectronics (ICM'20) and Fourth International Conference on Multimedia Computing, Networking and Applications (MCNA'20))




## Important notes

Every group must create a private repo with 4 members + add cmpe295 as an admin.

Please keep the same directory structure, keep things clean and tidy so that we do not need to search things around.  
 
I would like you to stick to the following directory structure. You need to use git and push your files to the respective directories. “pub” folder would include your publishable report in IEEE latex format. You have to learn latex. 
 
**Directory**  
> **--2021f_groupNumber** (alphabetically first group member's lastname)

> > **--doc** (CMPE295 canvas documents, workbooks, presentation, videos and reports)

> > **--pub** (publication, IEEE formatted project report)

> > > **--fig** (your drawing files, svf, visio, AI, etc.)

> > **--src** (your code goes in here)

> > **--ref** (references, papers, webpages)

> > **--misc** (miscellaneous)



